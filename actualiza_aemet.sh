#!/usr/bin/sh

# Verificar si la variable de entorno está definida
if [ -z "$XDG_CONFIG_HOME" ]; then
	# Si no está definida, usar un directorio predeterminado en el directorio de inicio del usuario
	config_dir="$HOME/.config"
else
	# Si está definida, utilizar el directorio especificado
	config_dir="$XDG_CONFIG_HOME"
fi

rm $config_dir/waybar/custom/diaria.json
rm $config_dir/waybar/custom/horaria.json

python $config_dir/waybar/custom/aemet.py
