# AEMET en Waybar

## Como instalarlo 

Dentro del directorio de configuración de Waybar HOME/.config/waybar, deberemos crear la carpeta "custom" HOME/.config/waybar/custom.

Allí añadimos los ficheros de interés: 
- aemet.py
- ajustes_aemet.json 
- actualiza_aemet.sh 
- abrir_url.py 

## Configuración de Waybar 

Añadimos el módulo "custom" llamado aemet en el lugar de la barra de Waybar que nos interese. Configuraremos el módulo como se desee, mi configuración:

```
/// Módulo para la previsión del tiempo
"custom/aemet": {
    "exec": "python ~/.config/waybar/custom/aemet.py",
    "restart-interval": 60,
    "return-type": "json",
    "format-alt": "{alt}",
    "on-click-middle": "python ~/.config/waybar/custom/abrir_url.py",
    "on-click-right": "sh $HOME/.config/waybar/custom/actualiza_aemet.sh",
},
```

Con esta configuración, con el botón derecho del ratón, se actualizará el módulo (necesario si cambiamos el municipio de inteŕes) y con el botón central abriremos la página de AEMET que consulta el municipio. 

Si queremos cambiar los colores según el tiempo que haga, podemos hacerlo en el fichero de configuración de Waybar llamado style.css. Mi configuración:

```
#custom-aemet.11 {
  color: #ffff00 
}
#custom-aemet.11n {
  color: #f8f8f2 
}
#custom-aemet.23, #custom-aemet.24, #custom-aemet.25, #custom-aemet.26{
  color: #5aacd0 
}
#custom-aemet.23n, #custom-aemet.24n, #custom-aemet.26n{
  color: #5aaca5  
}
#custom-aemet.default {
  color: #f8f8f2 
}
```

## Elegir localidad y clave de API AEMET

Se editará el fichero ajustes_aemet.json, que está dentro de la carpeta "custom" y cambiaremos el código del campo "codigo_ine". Hay dos de ejemplo para elegir, Móstoles y Piélagos, pero podemos elegir el que queramos:

[Códigos INE](https://ine.es/daco/daco42/codmun/codmunmapa.htm)

Además, tendremos que pedir a AEMET una clave para consultar los datos a través de la API, la pondremo sen el campo "api_key" del mismo fichero:

[Obtención de clave para la API](https://opendata.aemet.es/centrodedescargas/obtencionAPIKey)

