#!/usr/bin/env python

# abrir_url.py
import sys
import webbrowser
import json 
import os


# Obtenemos la URL del fichero json creado por aemet.py y enviado a waybar 
with open(os.path.expanduser("~/.config/waybar/custom/out_data.json")) as archivo:
    datos_leidos = json.load(archivo)

# Abrir la URL en el navegador predeterminado
webbrowser.open(datos_leidos["url"])
