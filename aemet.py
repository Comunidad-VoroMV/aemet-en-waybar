#!/usr/bin/env python 

import json 
import requests
import os  
from datetime import datetime
#from datatime import datatime,timedelta
#from pahtlib import Path 

# Iconos de estado del cielo 

weather_icons = {
    "11": "󰖙",
    "11n":"󰖔",
    "12": "󰖕",
    "12n":"󰼱",
    "13": "󰖕",
    "13n":"󰼱",
    "14": "",
    "14n":"",
    "15": "",
    "15n": "",
    "16": "",
    "16n": "",
    "17": "",
    "17n":"",
    "43": "󰼳",
    "43n":"󰼱",
    "44": "󰼳",
    "44n":"󰼱",
    "45": "󰖗",
    "45n": "󰖗",
    "46": "󰖗",
    "46n": "󰖗",
    "23": "",
    "23n":"",
    "24": "",
    "24n":"",
    "25": "",
    "25n": "",
    "26": "",
    "26n": "",
    "71": "󰼴",
    "71n":"",
    "72": "󰖘",
    "72n":"󰖘",
    "73": "󰖘",
    "73n": "󰖘",
    "74": "󰖘",
    "74n": "󰖘",
    "33": "󰼴",
    "33n":"",
    "34": "",
    "34n":"",
    "35": "󰼶",
    "35n": "󰼶",
    "36": "󰼶",
    "36n": "󰼶",
    "51": "",
    "51n":"",
    "52": "",
    "52n":"",
    "53": "",
    "53n": "",
    "54": "",
    "54n": "",
    "61": "",
    "61n":"",
    "62": "",
    "62n":"",
    "63": "",
    "63n": "",
    "64": "",
    "64n": "",
    "81": "󰖑",
    "81n": "󰖑",
    "82": "󰼰",
    "82n": "󰼰",
    "83": "󰼰",
    "83n": "󰼰",
    "":   "",
}

wind_icons = {
    "N":  "",
    "NE": "󰦸",
    "E":  "",
    "SE": "󰧄",
    "S" : "",
    "SO": "󰧆",
    "O":  "",
    "NO": "󰦺",
    "C": "",
    "": "",
}

wind_icons_alt = {
    "N":  "",
    "NE": "",
    "E":  "",
    "SE": "",
    "S" : "",
    "SO": "",
    "O":  "",
    "NO": "",
}

#Obtenemos de AEMET la predicción por horas Varibles de entrada:
#   -api_key - Clave para acceder a los datos de la API 
#   -codigo_municipio - código del municipio a consultar, ejemplo 39052 = Píélagos 
def obtener_prediccion_horaria(api_key, codigo_municipio):
    url = f"https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/horaria/{codigo_municipio}"
    headers = {
        "cache/-control": "no-cache"
    }
    querystring = {"api_key":api_key}

    response = requests.request("GET",url, headers=headers,params=querystring)

    data = response.json()

    prediccion = requests.get(data["datos"]).json()

    return prediccion 

#Obtenemos de AEMET la predicción por días. Varibles de entrada:
#   -api_key - Clave para acceder a los datos de la API 
#   -codigo_municipio - código del municipio a consultar, ejemplo 39052 = Píélagos 
def obtener_prediccion_diaria(api_key, codigo_municipio):
    url = f"https://opendata.aemet.es/opendata/api/prediccion/especifica/municipio/diaria/{codigo_municipio}"
    headers = {
        "cache-control": "no-cache"
    }
    querystring = {"api_key":api_key}

    response = requests.request("GET",url, headers=headers,params=querystring)

    data = response.json()

    prediccion = requests.get(data["datos"]).json()

    return prediccion 

def guardar_fichero_json(json_data, file_name): # Guardamos un fichero json en un archivo 
    with open(path+file_name,"w") as archivo:
        json.dump(json_data, archivo)

def leer_fichero_json(file_name): # Leemos un fichero json  y devolvemos el contenido 
    with open(path+file_name,"r") as archivo:
        datos_leidos = json.load(archivo)

    return datos_leidos # devolución del contenido 

def convertir_str_time(str): # Conversión de las variables tiempo de los ficheros AEMET a variables tipo datetime de Python 
    time = datetime.strptime(str, "%Y-%m-%dT%H:%M:%S")
    return time 


# Actualiza los datos de la predicción y guarda dos ficheros. 
#   Variables de entrada:
#   -api_key - Clave para acceder a los datos de la API 
#   -codigo_municipio - código del municipio a consultar, ejemplo 39052 = Píélagos 
#   Ficheros:
#   -"horaria.json" - predicción horaria 
#   -"diaria.json" - predicción diaria 
def actualiza_prediccion(api_key, codigo_municipio):
     # Obtén la predicción meteorológica por horas
    prediccion_horaria = obtener_prediccion_horaria(api_key, codigo_municipio)

    # Obtén la predicción meteorológica diaria 
    prediccion_diaria = obtener_prediccion_diaria(api_key, codigo_municipio)

    # Guarda los datos de la predicción en un fichero

    guardar_fichero_json(prediccion_horaria,"horaria.json")
    guardar_fichero_json(prediccion_diaria,"diaria.json")


path = os.path.expanduser("~/.config/waybar/custom/")  # Path de ubicación de los archivos

# Configura tu clave API de AEMET

config = leer_fichero_json("ajustes_aemet.json")
api_key = config[0]["api_key"]
codigo_municipio = config[0]["codigo_ine"]


# Leemos los ficheros que tenemos guardados
#
if not (os.path.exists(path+"horaria.json") and os.path.exists(path+"diaria.json")):
    actualiza_prediccion(api_key, codigo_municipio)

prediccion_horaria = leer_fichero_json("horaria.json")
prediccion_diaria = leer_fichero_json("diaria.json")

elaborado = prediccion_diaria[0]["elaborado"]

#Creamos la variable ahora con el timepo actual para compararlo con la hora del infomre de AEMET 
ahora = datetime.now()
hora_now = ahora.hour 

if hora_now < 6:
    rango_now = 0 
elif hora_now < 12: 
    rango_now = 1 
elif hora_now < 18:
    rango_now = 2 
else: 
    rango_now = 3 

informe = convertir_str_time(elaborado) 

primer_dia = convertir_str_time(prediccion_diaria[0]["prediccion"]["dia"][0]["fecha"])

if primer_dia.day == ahora.day:
    dia_0 = 0 
else:
    dia_0 = 1

#Comparamos si estamos en el mismo día/mes/año. Si no es así, actualizamos los ficheros y volvemos a leer 
if (ahora.day != informe.day) or (ahora.month != informe.month) or (ahora.year != informe.year) or (dia_0==1):
    print("ACTUALIZAMOS PREVISIÓN")
    diff = ahora - informe 
    actualiza_prediccion(api_key, codigo_municipio)
    prediccion_horaria = leer_fichero_json("horaria.json")
    prediccion_diaria = leer_fichero_json("diaria.json")
else:
    #Vemos la diferencia que hay entre la hora del informe y la actual, si es mayor de 6 horas, actualizamos los ficheros y volvemos a leer 
    diff = ahora-informe

    if ( (diff.total_seconds()/3600) > 6):  
        actualiza_prediccion(api_key, codigo_municipio)
        prediccion_horaria = leer_fichero_json("horaria.json")
        prediccion_diaria = leer_fichero_json("diaria.json")

if primer_dia.day == ahora.day:
    dia_0 = 0 
else:
    dia_0 = 1

# Vamos a leer las distintas variables de interés de la predicción de ahora 

prediccion_hora = prediccion_horaria[0]["prediccion"]["dia"][dia_0] 

control_estadoCielo = 0 
control_precipitacion = 0 
control_temperatura = 0 
control_sensTermica = 0 
control_humedadRelativa =0 
control_viento  = 0 

for n in range(48): 
    if (control_estadoCielo == 0):
        control_hora = int(prediccion_hora["estadoCielo"][n]["periodo"])
        if (hora_now == control_hora):
            prediccion_hora_estadoCielo = prediccion_hora["estadoCielo"][n]
            control_estadoCielo = 1 
  
    if (control_precipitacion == 0):
        control_hora = int(prediccion_hora["precipitacion"][n]["periodo"])
        if (hora_now == control_hora):
            prediccion_hora_precipitacion = prediccion_hora["precipitacion"][n] 
            control_precipitacion = 1 

    if (control_temperatura == 0):
        control_hora = int(prediccion_hora["temperatura"][n]["periodo"])
        if (hora_now == control_hora):
            prediccion_hora_temperatura = prediccion_hora["temperatura"][n] 
            control_temperatura = 1 

    if (control_sensTermica == 0):
        control_hora = int(prediccion_hora["sensTermica"][n]["periodo"])
        if (hora_now == control_hora):
            prediccion_hora_sensTermica = prediccion_hora["sensTermica"][n]
            control_sensTermica = 1 

    if (control_humedadRelativa == 0):
        control_hora = int(prediccion_hora["humedadRelativa"][n]["periodo"])
        if (hora_now == control_hora):
            prediccion_hora_humedadRelativa = prediccion_hora["humedadRelativa"][n]
            control_humedadRelativa = 1 

    if (control_viento == 0):
        control_hora = int(prediccion_hora["vientoAndRachaMax"][n]["periodo"])
        if (hora_now == control_hora):
            prediccion_hora_viento = prediccion_hora["vientoAndRachaMax"][n]
            control_viento = 1 

prediccion_hora_orto = prediccion_hora["orto"]
prediccion_hora_ocaso = prediccion_hora["ocaso"] 
        
# Vamos a leer las distintas variables de interés de la predicción diaria 

enlace = prediccion_diaria[0]["origen"]["enlace"] # URL a la predicción de la AEMET del municipio 
nombre_muni = prediccion_diaria[0]["nombre"] 

prediccion_dia = prediccion_diaria[0]["prediccion"]["dia"] 

# Iniciamos las variables 
prediccion_dia_probPrecipitacion = [ ]
prediccion_dia_estadoCielo = [ ]
prediccion_dia_viento = [ ]
prediccion_dia_temp_max = [ ]
prediccion_dia_temp_min = [ ]
prediccion_dia_temp_dato = [ ]
prediccion_dia_humedadRelativa_max = [ ]
prediccion_dia_humedadRelativa_min = [ ]
prediccion_dia_humedadRelativa_dato = [ ]
prediccion_dia_sensTermica_max = [ ]
prediccion_dia_sensTermica_min = [ ]
prediccion_dia_sensTermica_dato = [ ]
prediccion_dia_fecha = [ ]
prediccion_dia_uvMax = [ ]

icon = weather_icons[prediccion_hora_estadoCielo["value"]]
status = prediccion_hora_estadoCielo["descripcion"]

tooltip_text = str.format(
    "{:^110}\n\n{:^100}\n\n{}\n{}\n\n{}\n{}\n\n{}\n{}\n{}",
    f'<span size="xx-large">{icon} - {status}</span>',
    f'<span size="xx-large"> {prediccion_hora_temperatura["value"]} 󰔄\t\t{wind_icons[prediccion_hora_viento["direccion"][0]]}\t{prediccion_hora_viento["velocidad"][0]} km/h</span>',
    f'<span size="large">{nombre_muni}</span>',
    f'<span size="large"><i>{status}</i></span>',
    f'<span size="medium">\t{ahora.strftime("%H:%M")}</span>',
    f'<span size="medium">󰖜\t{prediccion_hora_orto}\t\t󰖛\t{prediccion_hora_ocaso}</span>',
    f'<span size="medium">Sensación térmica:\t\t{prediccion_hora_sensTermica["value"]}º</span>',
    f'<span size="medium">Temperatura: \t\t\t{prediccion_hora_temperatura["value"]}º</span>',
    f'<span size="medium">Humedad relativa:\t\t{prediccion_hora_humedadRelativa["value"]}%</span>',
)


forecast_txt = str.format (
     f'<span size="medium"><i>\n\nPronóstico:</i>\n\t \t\t    󰔄 \t\t  Sens.󰔄 \t\t     \t\t   󰖝  \t\tUV  \n─────────────────────────────────────────────────────────────────────────────────</span>',
)

for dia in range(7):

    prediccion_dia_probPrecipitacion.append(prediccion_dia[dia]["probPrecipitacion"])
    prediccion_dia_estadoCielo.append(prediccion_dia[dia]["estadoCielo"])
    prediccion_dia_viento.append(prediccion_dia[dia]["viento"])

    prediccion_dia_temp= prediccion_dia[dia]["temperatura"]
    prediccion_dia_temp_max.append(prediccion_dia_temp["maxima"])
    prediccion_dia_temp_min.append(prediccion_dia_temp["minima"])
    prediccion_dia_temp_dato.append(prediccion_dia_temp["dato"])

    prediccion_dia_humedadRelativa= prediccion_dia[dia]["humedadRelativa"]
    prediccion_dia_humedadRelativa_max.append(prediccion_dia_humedadRelativa["maxima"])
    prediccion_dia_humedadRelativa_min.append(prediccion_dia_humedadRelativa["minima"])
    prediccion_dia_humedadRelativa_dato.append(prediccion_dia_humedadRelativa["dato"])

    prediccion_dia_sensTermica= prediccion_dia[dia]["sensTermica"]
    prediccion_dia_sensTermica_max.append(prediccion_dia_sensTermica["maxima"])
    prediccion_dia_sensTermica_min.append(prediccion_dia_sensTermica["minima"])
    prediccion_dia_sensTermica_dato.append(prediccion_dia_sensTermica["dato"])

    prediccion_dia_fecha.append(convertir_str_time(prediccion_dia[dia]["fecha"]))

    if dia < 5: 
        prediccion_dia_uvMax.append(prediccion_dia[dia].get("uvMax"), )

    if dia < 2: 
        for x in range(3):
            if x == 1: 
                estadoCielo = prediccion_dia_estadoCielo[dia][0]
                probPrecipitacion = prediccion_dia_probPrecipitacion[dia][0]
            del prediccion_dia_probPrecipitacion[dia][0]
            del prediccion_dia_estadoCielo[dia][0] 
            del prediccion_dia_viento[dia][0] 
    elif dia < 4:
            if x == 0:
                estadoCielo = prediccion_dia_estadoCielo[dia][0]
                probPrecipitacion = prediccion_dia_probPrecipitacion[dia][0]
            del prediccion_dia_probPrecipitacion[dia][0]
            del prediccion_dia_estadoCielo[dia][0] 
            del prediccion_dia_viento[dia][0] 
    else:
        estadoCielo = prediccion_dia_estadoCielo[dia][0]
        probPrecipitacion = prediccion_dia_probPrecipitacion[dia][0]
   
    forecast_txt = forecast_txt + str.format (
        f'\n<span size="medium"> {prediccion_dia_fecha[dia].strftime("%d/%m/%Y")} \t',
    )
    forecast_txt = forecast_txt + str.format (
        f'{  prediccion_dia_temp_min[dia]} / {prediccion_dia_temp_max[dia]}\t',
    )
    forecast_txt = forecast_txt + str.format( 
        f'{prediccion_dia_sensTermica_min[dia]} / {prediccion_dia_sensTermica_max[dia]}\t\t',
    )
    forecast_txt = forecast_txt + str.format( 
        f'{probPrecipitacion["value"]}%    \t',
    )
    if prediccion_dia_viento[dia][0]["velocidad"]>29 or prediccion_dia_viento[dia][0]["velocidad"] in (10,15): 
        forecast_txt = forecast_txt + str.format(
             f'{wind_icons[prediccion_dia_viento[dia][0]["direccion"]]}\t{prediccion_dia_viento[dia][0]["velocidad"]}\t', 
        )
    else:
        forecast_txt = forecast_txt + str.format(
             f'{wind_icons[prediccion_dia_viento[dia][0]["direccion"]]}\t{prediccion_dia_viento[dia][0]["velocidad"]}\t', 
        )


    if dia < 5: 
        forecast_txt = forecast_txt + str.format (
           f'{prediccion_dia_uvMax[dia]}\t', 
        )
    else:
        forecast_txt = forecast_txt + str.format (f'\t')
 
    forecast_txt = forecast_txt + str.format (
        f'{weather_icons[estadoCielo["value"]]} - {estadoCielo["descripcion"]} </span>',
    )

    if dia == 0:
        rango_inf = rango_now 
    else:
        rango_inf = 0 

    if dia < 2:
        for x in range(rango_inf, 4): 
            forecast_txt = forecast_txt + str.format (
                f'\n\t<span size="medium"> {prediccion_dia_viento[dia][x]["periodo"]}\t',
            )
            forecast_txt = forecast_txt + str.format (
                f'{" "}{prediccion_dia_temp_dato[dia][x]["value"]}\t\t',
            )
            forecast_txt = forecast_txt + str.format( 
                f'{" "}{prediccion_dia_sensTermica_dato[dia][x]["value"]}\t\t\t',
            )
            forecast_txt = forecast_txt + str.format( 
                f'{prediccion_dia_probPrecipitacion[dia][x]["value"]}%    \t',
            )
            forecast_txt = forecast_txt + str.format(
                f'{wind_icons[prediccion_dia_viento[dia][x]["direccion"]]} \t {prediccion_dia_viento[dia][x]["velocidad"]}\t\t', 
            )
            forecast_txt = forecast_txt + str.format (
                f'{ weather_icons[prediccion_dia_estadoCielo[dia][x]["value"]]} - {prediccion_dia_estadoCielo[dia][x]["descripcion"]} </span>',
            )
    elif dia < 4:
        for x in range(2): 
            forecast_txt = forecast_txt + str.format (
                f'\n\t<span size="medium"> {prediccion_dia_viento[dia][x]["periodo"]}\t',
            )
            forecast_txt = forecast_txt + str.format (
                f'{" "}{prediccion_dia_temp_max[dia]}\t\t',
            )
            forecast_txt = forecast_txt + str.format( 
                f'{" "}{prediccion_dia_sensTermica_max[dia]}\t\t\t',
            )
            forecast_txt = forecast_txt + str.format( 
                f'{prediccion_dia_probPrecipitacion[dia][x]["value"]}%    \t',
            )
            forecast_txt = forecast_txt + str.format(
                 f'{wind_icons[prediccion_dia_viento[dia][x]["direccion"]]}\t{prediccion_dia_viento[dia][x]["velocidad"]}\t\t', 
            )
            forecast_txt = forecast_txt + str.format (
                f'{weather_icons[prediccion_dia_estadoCielo[dia][x]["value"]]} - {prediccion_dia_estadoCielo[dia][x]["descripcion"]}</span>',
            )

tooltip_text = tooltip_text + forecast_txt 

# Datos de salida para el módulo de waybar
out_data = {
    "text": f'  {icon} {prediccion_hora_temperatura["value"]}º  ',
    "alt":  f'  {status} ({prediccion_hora_temperatura["value"]}º)  ',
    "tooltip": tooltip_text,
    "url": f"{enlace}", 
    "class": prediccion_hora_estadoCielo["value"]
}


print(json.dumps(out_data))

guardar_fichero_json(out_data,"out_data.json")

